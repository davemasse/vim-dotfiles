execute pathogen#infect()

" Enable syntax highlighting
syntax on

filetype plugin indent on

" Airline color scheme
let g:airline_theme = 'dark'

" General VIM color scheme
colorscheme mustang

" Automatically indent based on syntax
set autoindent
" Allow delete key on Mac to work like backspace
set backspace=2
" Prevent saving twice and causing grunt to run a second time
set backupcopy=yes
set backupdir=$HOME/.vim/tmp
" Use the indent from the previous line
set copyindent
set directory=$HOME/.vim/tmp
set fileencoding=utf8
" Hightlight matching search terms
set hlsearch
" Ignore case in searches
set ignorecase
" Start searching right away while typing
set incsearch
" Allow airline to display properly
set laststatus=2
set expandtab
" Show line numbers
set number
" Let F2 toggle paste functionality
set pastetoggle=<F2>
set shiftwidth=4
"set smartindent
" Open new document below when splitting horizontally
set splitbelow
" Open new document to the right when splitting vertically
set splitright
set tabstop=4

" Disable Markdown folding
let g:vim_markdown_folding_disabled=1

" Python-mode
let g:pymode_folding = 0
let g:pymode_lint = 1
let g:pymode_lint_checkers = ['pep8']
let g:pymode_lint_ignore = "E501"
let g:pymode_lint_message = 1
let g:pymode_lint_on_write = 1
autocmd FileType python let g:syntastic_check_on_wq = 0

let g:syntastic_php_checkers=['php']

" Key mappings
" Normal mode
"nnoremap <C-j> :m .+1<CR>==
"nnoremap <C-k> :m .-2<CR>==

" Insert mode
"inoremap <C-j> <ESC>:m .+1<CR>==gi
"inoremap <C-k> <ESC>:m .-2<CR>==gi

" Visual mode
"vnoremap <C-j> :m '>+1<CR>gv=gv
"vnoremap <C-k> :m '<-2<CR>gv=gv

nnoremap <C-j> m`o<Esc>``
nnoremap <C-k> m`O<Esc>``

:command Space set shiftwidth=4 tabstop=4 expandtab
:command Space2 set shiftwidth=2 tabstop=2 expandtab
:command Tab set shiftwidth=4 tabstop=4 noexpandtab
