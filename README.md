# Download git submodules for this repository

Option 1: Clone using recursive flag

    git clone --recursive [repo_url]

Option 2: Download submodules

    git submodule init
    git submodule update

# Copy or link .vimrc

Copy or link the .vimrc file to ~/.vimrc

    cp .vimrc ~/

OR

    ln -s .vimrc ~/

# Copy or link .vim directory

Copy or link the .vim directory to ~/.vim

    cp .vim ~/

OR

    ln -s .vim ~/

# Create temp directory for vim backups

    mkdir -p ~/.vim/tmp

# Enable color syntax highlighting

> ~/.bash_profile

    export TERM="xterm-256color"

# Disable refresh in iTerm2

Avoid accidentally refreshing the screen, which messes up the vim session

> Preferences > Keys > Bind ⌘r (Command + r) to Ignore
